import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import ViteComponents, { ElementPlusResolver } from 'vite-plugin-components'
import Pages from 'vite-plugin-pages'
import Layouts from 'vite-plugin-vue-layouts'
import WindiCSS from 'vite-plugin-windicss'
import ViteIcons, { ViteIconsResolver } from 'vite-plugin-icons'
import svgLoader from 'vite-svg-loader'

export default defineConfig({
  server: {
    proxy: {
      '/api': {
        target: 'http://localhost:8080',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      },
    },
    open: true
  },
  plugins: [
    vue(),
    ViteComponents({
      customComponentResolvers: [
        ElementPlusResolver(),
        ViteIconsResolver()
      ]
    }),
    Pages({
      extensions: ['vue', 'md'],
    }),
    Layouts(),
    WindiCSS(),
    ViteIcons(),
    svgLoader()
  ]
})
