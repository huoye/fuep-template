# fuep-template
这是一个基于vue3和vite2搭建的工程模版，我已经为你配置好了一切，你可以直接在这个模版中使用fuep的物料。

[在线体验](http://admin.fuep.net)

### 快速开始
请注意你的node版本，推荐12+
```
npm i
npm run dev
```

### 引入的库
- vue3.0
- element plus
- vuex
- vue-router
- axios

### 特性
- 支持windicss [vite-plugin-windicss](https://www.npmjs.com/package/vite-plugin-windicss)
- 路由自动生成 [vite-plugin-pages](https://www.npmjs.com/package/vite-plugin-pages)
- 支持layout [vite-plugin-vue-layouts](https://www.npmjs.com/package/vite-plugin-vue-layouts)
- 自动导入components中的组件 [vite-plugin-components](https://www.npmjs.com/package/vite-plugin-components)
- 支持<script setup>
- 支持丰富的图标 [vite-plugin-icons](https://www.npmjs.com/package/vite-plugin-icons)

### vscode插件
- WindiCSS IntelliSense，用于windicss的样式提示
- 如果你使用了<script setup>，则需要添加Volar

