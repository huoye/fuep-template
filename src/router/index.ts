import { createRouter, createWebHistory } from 'vue-router'
import routes from 'virtual:generated-pages';
import { setupLayouts } from 'layouts-generated'

const routesWithLayout = setupLayouts(routes)

export default createRouter({
    history: createWebHistory(),
    routes: routesWithLayout
})