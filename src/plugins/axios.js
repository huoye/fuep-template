import axios from "axios";

let config = {
    baseURL: import.meta.env.VITE_APP_API,
}

const _axios = axios.create(config)

_axios.interceptors.request.use(
    function (config) {
        let token = localStorage.getItem('token')
        config.headers["token"] = token || '';
        return config
    },
    function (error) {
        return Promise.reject(error)
    }
)

_axios.interceptors.response.use(
    function (response) {
        return response
    },
    function (error) {
        return Promise.reject(error)
    }
)

export default _axios