import { createApp } from 'vue'
import App from './App.vue'
import store from './store/index'
import router from './router/index'
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css';
import 'virtual:windi.css'
import axios from './plugins/axios.js'

const app = createApp(App);

/**
 * 
 * In SFC,you can visit axios by:
 * import { getCurrentInstance } from 'vue'
 * const { proxy } = getCurrentInstance()  
 * proxy.$axios()
 * 
 */
app.config.globalProperties.$axios = axios;
app.use(router).use(store).use(ElementPlus).mount('#app')
