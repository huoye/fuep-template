import { createStore } from 'vuex'
import createPersistedState from "vuex-persistedstate";

const dataState = createPersistedState({
    paths: ['breadcrumb']
})

export default createStore({
    state: {
        breadcrumb: [
            {
                name: 'Home',
                level: 1,
                path: '/'
            }
        ],
    },
    mutations: {
        changeBreadcrumb: function (state, payload) {
            state.breadcrumb = payload
        }
    },
    plugins: [dataState]
})